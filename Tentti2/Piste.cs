﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tentti2
{
    class Piste
    {
        private string Name { get; set; }
        private double X { get; set; }

        private double Y { get; set; }

        public string getName() {
            return Name;
        }
        public double getX() { 
            return X;
       }
        public double getY()
        {
            return Y;
        }
        public void setName(string name)
        {
            Name = name; ;
        }
        public void setX(double x)
        {
            X = x;
        }      
        public void setY(double y)
        {
            Y = y;
        }
        public Piste(string name, double x, double y)
        {
            Name = name;
            X = x;
            Y = y;
        }
        public Piste()
        {
            Name = "";
            X = 0;
            Y = 0;
        }

        public override string ToString()
        {
            return String.Format("nimi: '{0}', X: {1}, Y; {2}", Name, X, Y);
        }
    }
}
