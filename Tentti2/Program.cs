﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tentti2
{
    class Program
    {
        static void Main(string[] args)
        {

            List<Piste> pisteet = new List<Piste>();

            Piste p1 = new Piste("testi1", 1, 2);
            Piste p2 = new Piste("testi2", 3, 4);
            pisteet.Add(p1);
            pisteet.Add(p2);

            string jsonpisteet = JsonConvert.SerializeObject(pisteet, Formatting.Indented);

            Console.WriteLine(jsonpisteet);


            //Bin data

            try { 
                FileStream stream = new FileStream(@"C:\tmp\bindataTentti.bin", FileMode.Open);
            
                BinaryReader br = new BinaryReader(stream);
                br.BaseStream.Seek(0, SeekOrigin.Begin);

                while (br != null && br.BaseStream.Position < br.BaseStream.Length)
                {
                    string name = br.ReadString();
                    double x = br.ReadDouble();
                    double y = br.ReadDouble();
                    pisteet.Add(new Piste(name, x, y));
                }

                br.Close();
                stream.Close();
            }
            catch(Exception e)
            {
                Console.WriteLine("exception");
            }

            pisteet.ForEach(p => Console.WriteLine(p));

            jsonpisteet = JsonConvert.SerializeObject(pisteet, Formatting.Indented);

            Console.WriteLine(jsonpisteet);
        }
    }
}
